let incorrect = document.getElementById("incorrect");

document.getElementById("inputId").addEventListener("focus", function () {
    this.setAttribute("class", "input-on-focus");
});
document.getElementById("inputId").addEventListener("blur", function () {
    let inputValue = document.getElementById("inputId").value;
    if (inputValue > 0){
        if(!incorrect.classList.contains("d-none")){
            incorrect.classList.add("d-none")
        }

        this.setAttribute("class", "input-on-blur");
        const div = document.createElement("div");
        const span = document.createElement("span");
        const button = document.createElement("button");
        div.setAttribute("id","appBlock");
        span.innerText = `Current value ${inputValue}`;
        button.innerText = "x";
        button.setAttribute("id", " pId");
        button.classList.add("button-properties");
        div.appendChild(span);
        div.appendChild(button);
        let linkToNewBlock = document.getElementById("append-bank");
        linkToNewBlock.appendChild(div);
        button.addEventListener("click", function () {
           div.remove();
           document.getElementById("inputId").value = "";

        });

    }
    else{
        this.setAttribute("class", "red-border");
        incorrect.classList.remove("d-none");
    }
});
